#!/bin/bash

set -e # exit the script if any statement returns a non-true return value
       # Unfortunately it means you can't check $? as bash will never get to the
       # checking code if it isn't zero.
# command || { echo "command failed"; exit 1; }
# or
# if ! command; then echo "command failed"; exit 1; fi

NEW_BASE_URL=""
OLD_BASE_URL=""

BASEDIR=$(cd "`dirname $0`" 2>/dev/null && pwd)
# TEMPLATES=${BASEDIR}/templates
# README=${TEMPLATES}/README.md
# SCRIPT=${TEMPLATES}/migrate.sh


while [ $# -gt 0 ]; do
    case "$1" in
    -h|--help)          usage        exit 0                             ;;
    --new-base)         NEW_BASE_URL=$2;                              shift;;
    --old-base)         OLD_BASE_URL=$2;                                    shift;;
    *)                  usage                                           ;; 
    esac
    shift
done

# [ -z $NEW_BASE_HOST ] && {
#     echo 'You MUST specify the "--new-base <hostname>" parameter'
#     exit 1
# }

# SCC_REMOTE_NAME=`git remote -vv | grep git.scc.kit.edu | awk '{ print $1 }' | uniq`
# SCC_REMOTE=`git remote -vv | grep git.scc.kit.edu | awk '{ print $2 }' | uniq`
# NEW_ORIGIN_REMOTE=`echo ${SCC_REMOTE} | sed s%git@git.scc.kit.edu:%git@${NEW_BASE_HOST}:${NEW_TOP_GROUP}/%`
# LOCAL_BRANCHES=`git branch -vv | sed s/'*'// | awk '{ print $1 }'`
BRANCHES=`git branch -r | grep -v HEAD | awk '{ print $1 }' | sed s_origin/__`
# NEW_URL=`echo ${NEW_ORIGIN_REMOTE} | sed s%:%/% | sed s%git@%https://% | sed s/.git$//`

# echo "SCC_REMOTE_NAME:                  ${SCC_REMOTE_NAME}"
# echo "SCC_REMOTE:                       ${SCC_REMOTE}"
# echo "NEW_ORIGIN_REMOTE:                ${NEW_ORIGIN_REMOTE}"

REPO_NAME=`basename $PWD`
LOG="/tmp/migration.log"
[ -e $LOG ] && rm $LOG
echo "logging to $LOG"

MASTER_BRANCH=`git config --get init.defaultbranch`
# SELECTED_BRANCHES="${MASTER_BRANCH} prerel prerelease"
# SELECTED_BRANCHES="main master prerel prerelease"
SELECTED_BRANCHES="prerel prerelease"
echo "REPO_NAME: ${REPO_NAME}"
## list all branches that need modifications on other branches than prerel* here:
case ${REPO_NAME} in
    "oidc-agent-win-installer"|"buchungssytem-lastenkarle"|"clibs-packaging")
        SELECTED_BRANCHES="main"
    ;;
    "mailping"|"ci-voodoo"|"ansible"|"mteam.data.kit.edu"|"kit-urn-registry"|"ci-test")
        SELECTED_BRANCHES="master"
    ;;
    # o3as projects
    "mss"|"o3k8s"|"o3mocks"|"o3skim"|"o3skim/codemeta.json"|"o3sources"|"o3webapp")
        SELECTED_BRANCHES="main"
    ;;
    "o3docs"|"o3api"|"o3webapp-be"|"o3webapp-fe"|"o3as.data.kit.edu")
        SELECTED_BRANCHES="master"
    ;;
    "handbook")
        SELECTED_BRANCHES="main devel"
    ;;
    "nfdi-aai-doc")
        SELECTED_BRANCHES="master devel"
    ;;
    "python3-template")
        SELECTED_BRANCHES="main test marcus-flavor static_old_version cookiecutter_new_version"
    ;;
    *feudal*|"regapp-tools"|"ssh-key-retriever"|"sshkeyretrievergo"|"arpoc")
        echo aaa
        SELECTED_BRANCHES="master"
    ;;
    "hdf-info")
        SELECTED_BRANCHES="master policy-updates"
    ;;
    cookiecutter-web |DEEP-OC-deepaas_full |DEEP-OC-rnacontactmap |DEEP-OC-yolov8_api |demo-advanced-api |fasterrcnn_pytorch_api |GUI_rnacontactmap |mlflow_auth |mlflow-tutorial |object_detection_api_tensorflow |yolov8_api)
        SELECTED_BRANCHES="main master devel prerel"
    ;;
esac

# for BRANCH in ${BRANCHES}; do
for BRANCH in ${SELECTED_BRANCHES}; do
    git co ${BRANCH} >> $LOG 2>&1
    RV=$?
    [ $RV != 0 ] && {
        continue
    }
    echo -e "\nProcessing REPO ${REPO_NAME} -- branch ${BRANCH}"

    PAT="https://git.scc.kit.edu/m-team/ci-voodoo/raw/master/ci-include/"
    NEW="https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/"
    grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

    case ${REPO_NAME} in
        "ci-voodoo")
            PAT="https://git.scc.kit.edu/m-team/ci-voodoo/"
            NEW="https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i 2>/dev/null

            PAT="https://git.scc.kit.edu/m-team/ci-voodoo/raw/master/ci-include/"
            NEW="https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i 2>/dev/null

            PAT="https://git.scc.kit.edu/m-team/ci-voodoo.git"
            NEW="https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo.git"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i 2>/dev/null
        ;;&
        "motley_cue"|"oinit"|"oidc-agent")
            PAT="git.scc.kit.edu/feudal/"
            NEW="codebase.helmholtz.cloud/m-team/feudal/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i 2>/dev/null
        ;;&
        "oinit")
            PAT="git.scc.kit.edu/m-team/oinit"
            NEW="codebase.helmholtz.cloud/m-team/oinit/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "orpheus")
            PAT="git.scc.kit.edu/oidc/orpheus"
            NEW="codebase.helmholtz.cloud/m-team/oidc/orpheus"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "oidc-agent-win-installer")
            PAT="git.scc.kit.edu/m-team/oidc/oidc-agent-win-installer"
            NEW="codebase.helmholtz.cloud/m-team/oidc/oidc-agent-win-installer"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "oidc-agent")
            PAT="git.scc.kit.edu/m-team/oidc-agent"
            NEW="codebase.helmholtz.cloud/m-team/oidc/oidc-agent"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "mytoken")
            PAT="git.scc.kit.edu/m-team/oidc/mytoken"
            NEW="codebase.helmholtz.cloud/m-team/oidc/mytoken"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "buchungssytem-lastenkarle")
            PAT="https://git.scc.kit.edu/m-team/pse23/buchungssytem-lastenkarle"
            NEW="https://codebase.helmholtz.cloud/m-team/pse23/buchungssytem-lastenkarle"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "mailping")
            PAT="git.scc.kit.edu/m-team/mailping"
            NEW="codebase.helmholtz.cloud/m-team/tools/mailping"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "ansible")
            PAT="=git.scc.kit.edu"
            NEW="=codebase.helmholtz.cloud"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "clibs-packaging")
            PAT="https://git.scc.kit.edu/m-team/clibs-packaging"
            NEW="https://codebase.helmholtz.cloud/m-team/tools/packaging/clibs-packaging"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "mss" | "o3api" | "o3docs" | "o3k8s" | "o3mocks" | "o3skim" | "o3skim/codemeta.json" | "o3sources" | "o3webapp" | "o3webapp-be" | "o3webapp-fe")
            PAT="https://git.scc.kit.edu/synergy.o3as"
            NEW="https://codebase.helmholtz.cloud/m-team/o3as"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="git.scc.kit.edu:synergy.o3as/o3"
            NEW="codebase.helmholtz.cloud:m-team/o3as/o3"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="https://git.scc.kit.edu/synergy.o3as/o3"
            NEW="https://codebase.helmholtz.cloud/m-team/o3as/o3"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

        ;;&
        "o3as.data.kit.edu")
            PAT="https://git.scc.kit.edu/synergy.o3as"
            NEW="https://codebase.helmholtz.cloud/m-team/o3as"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "mteam.data.kit.edu")
            PAT="https://git.scc.kit.edu/m-team/mteam.data.kit.edu"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/mteam-data-kit-edu"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="https://git.scc.kit.edu/m-team"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "handbook")
            PAT="https://git.scc.kit.edu/m-team/putty/"
            NEW="https://codebase.helmholtz.cloud/m-team/tools/packaging/upstream/putty/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="https://git.scc.kit.edu/m-team/handbook/-/pipelines"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/handbook/-/pipelines"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "kit-urn-registry")
            PAT="https://git.scc.kit.edu/m-team/kit-urn-registry"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/kit-urn-registry"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "python3-template")
            PAT="https://git.scc.kit.edu/m-team/python3-template"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/python3-template"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "nfdi-aai-doc")
            PAT="https://git.scc.kit.edu/m-team/nfdi-aai-doc"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/nfdi-aai-doc"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="https://git.scc.kit.edu/m-team/web-doc/nfdi-aai-doc"
            NEW="https://codebase.helmholtz.cloud/m-team/web-doc/nfdi-aai-doc"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        *feudal*|"regapp-tools"|"ssh-key-retriever"|"sshkeyretrievergo")
            PAT="git.scc.kit.edu:feudal"
            NEW="codebase.helmholtz.cloud:m-team/feudal"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="git.scc.kit.edu/feudal"
            NEW="codebase.helmholtz.cloud/m-team/feudal"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "ci-test"):
            PAT="https://git.scc.kit.edu/m-team/ci-test"
            NEW="https://codebase.helmholtz.cloud/m-team/archive/ci-test"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "hdf-info")
            PAT="https://git.scc.kit.edu/m-team/hdf-info"
            NEW="https://codebase.helmholtz.cloud/m-team/archive/hdf-info"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        "arpoc")
            PAT="https://git.scc.kit.edu/m-team/"
            NEW="https://codebase.helmholtz.cloud/m-team/archive/"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&
        cookiecutter-web |DEEP-OC-deepaas_full |DEEP-OC-rnacontactmap |DEEP-OC-yolov8_api |demo-advanced-api |fasterrcnn_pytorch_api |GUI_rnacontactmap |mlflow_auth |mlflow-tutorial |object_detection_api_tensorflow |yolov8_api)
            echo "here"
            PAT="https://git.scc.kit.edu/m-team/ai"
            NEW="https://codebase.helmholtz.cloud/m-team/ai"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null

            PAT="git.scc.kit.edu:m-team/ai"
            NEW="codebase.helmholtz.cloud:m-team/ai"
            grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        ;;&

        # "")
            # echo "here"
        #     PAT=""
        #     NEW=""
        #     grep -irl ${PAT} | xargs sed s%${PAT}%${NEW}%g -i  2>/dev/null
        # ;;&
    esac
    git commit -am 'update links to point to new gitlab instance'  >> $LOG 2>&1
    git push

done

# Repos with links to personal projects:
# o3skim:
#     README.md:- [@V.Kozlov](https://git.scc.kit.edu/eo9869)
#     README.md:- [@T.Kerzenmacher](https://git.scc.kit.edu/px5501)
#     README.md:- [@B.Esteban](https://git.scc.kit.edu/zr5094)

# feudalAdapterLdf/ldf_adapter/backend/bwidm.py:See https://git.scc.kit.edu/simon/reg-app.
# DEEP-OC-yolov8_api/metadata.json:		"dockerfile_repo": "https://git.scc.kit.edu/se1131/DEEP-OC-yolov8_api",
#
