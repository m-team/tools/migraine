#!/bin/bash

IFS='
'
CURRENT_DIR=`pwd`
LOG="${CURRENT_DIR}/log-all-repos.log"
pushd . > /dev/null
[ -d repos ] || mkdir repos
cd repos

# git@git.scc.kit.edu:m-team/ai/ai4eosc_backend.git
# for i in `head $CURRENT_DIR/repos.list -n 2`; do
# for i in `cat $CURRENT_DIR/repos.list.really-failed`; do
for i in `cat $CURRENT_DIR/repos.list`; do
    repo=`echo $i | sed s/\ /_/g`
    echo -e "\n${repo}"
    [ -d ${repo} ] && {
        echo "${repo} exists: SKIPPING"
    }
    [ -d $i ] || {
        echo "git clone git@git.scc.kit.edu:${repo}.git ${repo}"
        git clone git@git.scc.kit.edu:${repo}.git  ${repo} >> $LOG 2>&1 
    }
    # make sure we have a local copy of all branches
    ( 
        cd ${repo}
        echo "PWD: $PWD"
        BRANCHES=`git branch -r | grep -v HEAD | awk '{ print $1 }' | sed s_origin/__`
        for BRANCH in ${BRANCHES}; do
            echo -e "Processing branch ${BRANCH}"
            git co ${BRANCH} >> $LOG 2>&1
        done
    )
done
unset IFS
popd > /dev/null
