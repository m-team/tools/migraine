#!/bin/bash
GL_DOMAIN="https://git.scc.kit.edu"
GL_TOKEN="9L6YLvieEKMdVwtZBsJx"
echo "" > gitlab_projects_urls.txt
for ((i=1; ; i+=1)); do
    contents=$(curl "$GL_DOMAIN/api/v4/projects?private_token=$GL_TOKEN&per_page=100&page=$i&owned=True")
    if jq -e '. | length == 0' >/dev/null; then 
       break
    fi <<< "$contents"
    echo "$contents" | jq -r '.[].ssh_url_to_repo' >> repos.ssh-urls
    echo "$contents" | jq -r '.[].path_with_namespace' >> repos.list
    echo "$contents" | jq . |\
        grep -E '(^    "id":|^    \"web_url")' \
        | sed s/'"id": '// \
        | sed s/'"web_url": '// \
        | sed '{N; s/,\n//}'\
        | sed s/'[",]'//g \
        >> repos.web_url
    echo "$contents" | jq .  >> repos.raw

done
