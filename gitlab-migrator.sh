#!/bin/bash

set -e # exit the script if any statement returns a non-true return value
       # Unfortunately it means you can't check $? as bash will never get to the
       # checking code if it isn't zero.
# set -x
# command || { echo "command failed"; exit 1; }
# or
# if ! command; then echo "command failed"; exit 1; fi

NEW_BASE_HOST=""
NEW_TOP_GROUP=""

GL_DOMAIN="https://git.scc.kit.edu"
GL_TOKEN="9L6YLvieEKMdVwtZBsJx"

BASEDIR=$(cd "`dirname $0`" 2>/dev/null && pwd)
TEMPLATES=${BASEDIR}/templates
README=${TEMPLATES}/README.md
SCRIPT=${TEMPLATES}/migrate.sh


while [ $# -gt 0 ]; do
    case "$1" in
    -h|--help)          usage        exit 0                             ;;
    --new-base)         NEW_BASE_HOST=$2;                              shift;;
    --new-top-group)    NEW_TOP_GROUP=$2;                                    shift;;
    *)                  usage                                           ;; 
    esac
    shift
done

[ -z $NEW_BASE_HOST ] && {
    echo 'You MUST specify the "--new-base <hostname>" parameter'
    exit 1
}

# make sure ${NEW_TOP_GROUP} ends with '/'
sanitize_slashes() {
    VALUE=$1
    # make sure VALUE does end with "/"
    [ "${VALUE: -1}" == "/" ] || {
        VALUE="${VALUE}/"
    }
    echo ${VALUE}
}
[ -z "$NEW_TOP_GROUP" ] || {
    NEW_TOP_GROUP=$(sanitize_slashes ${NEW_TOP_GROUP})
}


SCC_REMOTE_NAME=`git remote -vv | grep git.scc.kit.edu | awk '{ print $1 }' | uniq`
SCC_REMOTE=`git remote -vv | grep git.scc.kit.edu | awk '{ print $2 }' | uniq`
NEW_ORIGIN_REMOTE=`echo ${SCC_REMOTE} | sed s%git@git.scc.kit.edu:%git@${NEW_BASE_HOST}:${NEW_TOP_GROUP}%`
LOCAL_BRANCHES=`git branch -vv | sed s/'*'// | awk '{ print $1 }'`
SCC_BRANCHES=`git branch -r \
    | grep -v HEAD \
    | grep ${SCC_REMOTE_NAME} \
    | awk '{ print $1 }' \
    | sed s_${SCC_REMOTE_NAME}/__`
NEW_URL=`echo ${NEW_ORIGIN_REMOTE} | sed s%:%/% | sed s%git@%https://% | sed s/.git$//`

# get ID from file:
# REPO_NAME=`echo git@git.scc.kit.edu:m-team/feudal/test.git | awk -F: '{ print $2 }' | sed s/\.git//`
REPO_NAME=`git remote -vv \
    | grep git.scc.kit.edu \
    | awk '{ print $2 }' \
    | uniq \
    | awk -F: '{ print $2 }' \
    | sed s/\.git//\
    `
ID=`grep ${REPO_NAME}\$ ~/projects/migraine/repos.web_url | awk '{ print $1 }'`

echo "========================================================================"
pwd
echo "SCC_REMOTE_NAME:                  ${SCC_REMOTE_NAME}"
echo "SCC_REMOTE:                       ${SCC_REMOTE}"
echo "NEW_ORIGIN_REMOTE:                ${NEW_ORIGIN_REMOTE}"
echo "REPO_NAME:                        ${REPO_NAME}"
echo "ID:                               ${ID}"
echo "NEW_TOP_GROUP:                    ${NEW_TOP_GROUP}"

LOGDIR="/tmp/migration/${REPO_NAME}"
mkdir -p ${LOGDIR}
LOG="${LOGDIR}/migration.log"
[ -e $LOG ] && {
    rm $LOG
}
date >> $LOG
echo "logging to $LOG"

# unprotect branches of this repo
http --ignore-stdin DELETE "$GL_DOMAIN/api/v4/projects/${ID}/protected_branches/*?private_token=$GL_TOKEN" >> /tmp/httpie.log
#
for BRANCH in ${SCC_BRANCHES}; do
    echo -e "\nProcessing branch ${BRANCH}"
    # git co --track ${SCC_REMOTE_NAME}/${BRANCH} >> $LOG 2>&1
    git co ${BRANCH} >> $LOG 2>&1
    LAST_COMMIT_ON_CURRENT_BRANCH=`git log -n 1 | grep ^commit | awk '{ print $2 }' 2>/dev/null`

    ####
    # envsubst
    export BRANCH
    export LAST_COMMIT_ON_CURRENT_BRANCH
    export NEW_BASE_HOST
    export NEW_URL
    export NEW_TOP_GROUP
    export NEW_ORIGIN_REMOTE
    export USER
    # envsubst
    ####
    echo "PWD of rm -rf command: " >> $LOG
    rm -rf *
    rm -rf .gitlab*
    rm -rf .giti*
    rm -rf .[a-f]*
    rm -rf .[A-F]*
    rm -rf .[h-z]*
    rm -rf .[H-Z]*
    echo -n "    [Data removed] "
    cat ${README} | envsubst '$BRANCH,$LAST_COMMIT_ON_CURRENT_BRANCH,$NEW_BASE_HOST,$NEW_URL,$NEW_ORIGIN_REMOTE,$USER' > README.md
    cat ${SCRIPT} | envsubst '$BRANCH,$LAST_COMMIT_ON_CURRENT_BRANCH,$NEW_BASE_HOST,$NEW_URL,$NEW_ORIGIN_REMOTE' > migrate.sh
    chmod 755 migrate.sh
    # echo "${LAST_COMMIT_ON_CURRENT_BRANCH}" > hash-of-last-commit-on-this-branch.txt
    git add -A >> $LOG 2>&1
    git ci -am 'move to other gitlab instance'  >> $LOG 2>&1
    echo -n "[Migration info created] "
    git push >> $LOG 2>&1
    echo "[Pushed]"
done
# protect branches of this repo
http --ignore-stdin POST "$GL_DOMAIN/api/v4/projects/${ID}/protected_branches/?private_token=$GL_TOKEN"  "name=*" "push_access_level=0" "merge_access_level=0" "unprotect_access_level=40" >> /tmp/httpie.log

exit 0



################################################################################
# UNDO:
# SCC_BRANCHES=`git branch -r | grep -v HEAD | awk '{ print $1 }' | sed s_origin/__`
SCC_BRANCHES=`git branch -r \
    | grep -v HEAD \
    | grep ${SCC_REMOTE_NAME} \
    | awk '{ print $1 }' \
    | sed s_${SCC_REMOTE_NAME}/__`
for i in ${SCC_BRANCHES}; do  git co $i
    # git log --oneline --graph -n 10 | grep -A1 "move to other"
    commit=`git log --oneline --graph -n 10 | grep -A1 "move to other"| tail -n 1| awk '{ print $2 }'`
    echo $commit
    [ -z $commit ] || { 
        git reset --hard $commit
        git push -f 
    }
done


################################################################################
# undo from backup
GL_DOMAIN="https://git.scc.kit.edu"
GL_TOKEN="9L6YLvieEKMdVwtZBsJx"
REPO_NAME=`git remote -vv \
    | grep git.scc.kit.edu \
    | awk '{ print $2 }' \
    | uniq \
    | awk -F: '{ print $2 }' \
    | sed s/\.git//\
    `
ID=`grep ${REPO_NAME}\$ ~/projects/migraine/repos.web_url | awk '{ print $1 }'`

SCC_BRANCHES=`git branch -r \
    | grep -v HEAD \
    | grep ${SCC_REMOTE_NAME} \
    | awk '{ print $1 }' \
    | sed s_${SCC_REMOTE_NAME}/__`

echo "REPO_NAME:                        ${REPO_NAME}"
echo "ID:                               ${ID}"
http --ignore-stdin DELETE "$GL_DOMAIN/api/v4/projects/${ID}/protected_branches/*?private_token=$GL_TOKEN" >> /tmp/httpie.log

for i in ${SCC_BRANCHES}; do
    git co $i
    git push -f
done

